import { createTodo } from './todo/createTodo';
import { toggleTodo } from './todo/toggleTodo';
import { deleteTodo } from './todo/deleteTodo';
// ---------------------------------------------------
import { updateUserId } from './user/updateUserId';





export default {
  todo: {
    createTodo,
    toggleTodo,
    deleteTodo
  },
  user: {
    updateUserId
  }
}