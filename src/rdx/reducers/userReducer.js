export default function userReducer(state={}, payload) {
  switch (payload.type) {
    // -------------------------------------------------------
    case 'UPDATE_USER_ID':
      return {
        name: state.name,
        id: payload.id
      };
    // -------------------------------------------------------
    default:
      return Object.assign({}, state);
  }
}
