import todoReducer from './todoReducer';
import userReducer from './userReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  todos: todoReducer,
  user: userReducer
});

export default rootReducer;









