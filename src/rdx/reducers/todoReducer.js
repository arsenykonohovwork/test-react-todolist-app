export default function todoReducer(state=[], payload) {
  switch (payload.type) {
    // -------------------------------------------------------
    case 'CREATE_TODO':
      return [{
        id: getId(state),
        isCompleted: false,
        message: payload.message
      }, ...state];
    // -------------------------------------------------------
    case 'TOGGLE_TODO':
      return state.map((v)=>(v.id === payload.id)?Object.assign({},v,{isCompleted:!v.isCompleted}):v);
    // -------------------------------------------------------
    case 'DELETE_TODO':
      return state.filter((v)=>(v.id !== payload.id));
    // -------------------------------------------------------
    default:
      return state.slice();
  }
}

// =================================================================
function getId(state) {
  return state.reduce((p, v) => {
    return Math.max(v.id, p)
  }, -1) + 1;
}


