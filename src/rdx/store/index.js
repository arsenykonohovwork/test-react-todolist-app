import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from '../reducers';
import { createLogger } from 'redux-logger';

const logger = createLogger(
  // options here...
);
const wrappedCreateStore = compose(applyMiddleware(logger))(createStore);

export default (defaultState = {todos:[],user:{}}) => {
  return wrappedCreateStore(rootReducer, defaultState);
};







