import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
// ----------------------------------------------
import { Provider } from 'react-redux';
import Store from './rdx/store';
// ----------------------------------------------
import registerServiceWorker from './registerServiceWorker';

const initState = {
  todos: [{
    id: 0,
    isCompleted: false,
    message: 'Initial State for Todos'
  }],
  user: {
    name: 'John Doe',
    id: 85
  } 
};

const store = Store(initState);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();

