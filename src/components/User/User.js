import React, { Component } from 'react';
import './User.css';
import UserInfo from '../UserInfo/UserInfo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from '../../rdx/actions';


class User extends Component {
  render() {
    return (
      <div className="User">
        <h3>USER:</h3>
        <UserInfo actions={this.props.actions} info={this.props.user}/>
      </div>
    );
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(User);

// --------------------------------------------------------------
function mapStateToProps(state) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions.user, dispatch)
  };
}










