import React, { Component } from 'react';
import './TodoInput.css';


class TodoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
  }
  handleChange(e) {
    this.setState({value: e.target.value});
  }
  handleSubmit(e) {
    if (this.state.value.length) {
      this.props.actions.createTodo(this.state.value);
      this.setState({value: ''});
    }
    e.preventDefault();
  }
  render() {
    return (
      <div className='TodoInput flex'>
        <form onSubmit={this.handleSubmit.bind(this)} className="flex__container">
          <div className="flex__element grow-1">
            <input value={this.state.value} onChange={this.handleChange.bind(this)} 
              className='input' type='text' placeholder='Input here'/>
          </div>
        </form>
      </div>
    );
  }
}

export default TodoInput;





