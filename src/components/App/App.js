import React, { Component } from 'react';
import './App.css';
import Todo from '../Todo/Todo';
import User from '../User/User';


class App extends Component {
  render() {
    return (
      <div className="App">
        <User />
        <Todo />
      </div>
    );
  }
};
export default App;

