import React, { Component } from 'react';
import './TodoItem.css';


class TodoItem extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleToggle(e) {
    this.props.actions.toggleTodo(this.props.item.id);
    e.preventDefault();
  }
  handleDelete(e) {
    this.props.actions.deleteTodo(this.props.item.id);
    e.preventDefault();
  }
  render() {
    const up = <i className="icon fa fa-thumbs-o-up"></i>;
    const down = <i className="icon fa fa-thumbs-o-down"></i>;
    // -----------------------------
    return (
      <div className="flex TodoItem">
        <div className="flex__container">
          <div className="flex__element grid-20-2">
            {this.props.item.isCompleted ? up : down}
          </div>
          <div className="flex__element grid-20-10">
            <span>{this.props.item.message}</span>
          </div>
          <div className="flex__element grid-20-4">
            <button onClick={this.handleToggle} className="button full" type="button">Done</button>
          </div>
          <div className="flex__element grid-20-4">
            <button onClick={this.handleDelete} className="button full" type="button">DEL</button>
          </div>
        </div>
      </div>
    );
  }
}

export default TodoItem;





