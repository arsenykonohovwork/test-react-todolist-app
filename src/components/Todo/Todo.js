import React, { Component } from 'react';
import './Todo.css';
import TodoInput from '../TodoInput/TodoInput';
import TodoList from '../TodoList/TodoList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from '../../rdx/actions';


class Todo extends Component {
  render() {
    return (
      <div className="Todo">
        <TodoInput actions={this.props.actions} />
        <TodoList  actions={this.props.actions} todos={this.props.todos}/>
      </div>
    );
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Todo);

// --------------------------------------------------------------
function mapStateToProps(state) {
  return state;
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions.todo, dispatch)
  };
}









