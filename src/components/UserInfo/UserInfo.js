import React, { Component } from 'react';
import './UserInfo.css';

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    this.props.actions.updateUserId();
    e.preventDefault();
  }
  render() {
    return (
      <div className="UserInfo">
        <div className="flex">
          <div className="flex__container col">
            <div className="flex__element">Name: {this.props.info.name}</div>
            <div className="flex__element">ID: {this.props.info.id}</div>
            <div className="flex__element">
              <button onClick={this.handleClick} className="button full">Update ID</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default UserInfo;









