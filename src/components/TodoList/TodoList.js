import React, { Component } from 'react';
import './TodoList.css';
import TodoItem from '../TodoItem/TodoItem';


class TodoList extends Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    const todos = this.props.todos.map((v,i) => {
      return <TodoItem key={v.id} item={v} actions={this.props.actions} />;
    });
    return <div className="TodoList">{todos}</div>;
  }
}

export default TodoList;

